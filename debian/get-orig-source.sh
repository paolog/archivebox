#!/bin/bash
: <<=cut

=head1 DESCRIPTION

This script is called by uscan(1) as per "debian/watch" to export git
submodules as Multi Upstream Tarball (MUT) components.

=head1 COPYRIGHT

Copyright: 2018-2020 Dmitry Smirnov <onlyjob@member.fsf.org>

=head1 LICENSE

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

set -e
set -u

if [ "$1" = '--upstream-version' ]; then
    version="$2"
else
    printf "E: missing argument '--upstream-version'.\n" 1>&2
    exit 1
fi

GIT_RURL="https://github.com/ArchiveBox/ArchiveBox.git"
export XZ_OPT="-6v"
DEB_SOURCE="$( dpkg-parsechangelog -SSource )"

filename="$( readlink -f ../${DEB_SOURCE}_${version}.orig.tar.xz )"
#~[ -s "${filename}" ] || exit 1

## uscan's "mode=git" can only get tag or HEAD.
## Here we need current version hence we have to ask `dpkg-parsechangelog`
## and throw away version that `uscan` provided with '--upstream-version'
## argument hence redefining "$version" and "$filename".
rm -v "${filename}"
DEB_VERSION="$( dpkg-parsechangelog -SVersion )"
DEB_VERSION="${DEB_VERSION%%-*}"
version="$DEB_VERSION"
filename="$( readlink -f ../${DEB_SOURCE}_${version}.orig.tar.xz )"

## tarpack() takes two arguments:
##  1. directory to compress
##  2. tarball path/name
##  3. component/directory
tarpack() {
    ( cd "$1" && \
      find -L "$3" -xdev -type f -print | LC_ALL=C sort \
      | grep -v ^"$3"/.git/ \
      | XZ_OPT="-6v" tar -caf "$2" -T- --owner=root --group=root --mode=a+rX \
    )
}

work_dir="$( mktemp -d -t get-orig-source_${DEB_SOURCE}_XXXXXXXX )"
trap "rm -rf '${work_dir}'" EXIT

uversion="${version%%?dfsg*}"
uversion="${uversion##*git????????.}"
debdir="${DEB_SOURCE}_${DEB_VERSION}"

git clone ${GIT_RURL} "${work_dir}"/"${debdir}" ## not using "--recursive" to exclude submodules from the main tarball.
( cd "${work_dir}/"${debdir}"" && git checkout "v${uversion}" ) ## upstream appends 'v' to semantically-versioned tags.

tarpack "${work_dir}" "${filename}" "${debdir}"

## repack main tarball
mk-origtargz --package ${DEB_SOURCE} --version ${version} \
    --rename --repack --compression xz --directory .. \
    --copyright-file debian/copyright \
    "${filename}"

( cd "${work_dir}"/"${debdir}" && git submodule update --init )

## fetch components/submodules:
for I in $(cd "${work_dir}"/"${debdir}" && git submodule foreach --quiet 'echo $path'); do
    printf ":: Processing ${I}\n" 1>&2
    component=${I##*/}
    FN="$( readlink -f ../${DEB_SOURCE}_${version}.orig-${component}.tar.gz )"
    if [ ! -s "${FN}" ]; then
        tarpack "${work_dir}"/"${debdir}"/"${I}"/.. "${FN}" "${component}"

        mk-origtargz --package ${DEB_SOURCE} --version ${version} \
          --rename --repack --compression xz --directory .. \
          --copyright-file debian/copyright \
          --component ${component} \
        "${FN}"
    fi
done
#####

rm -rf "${work_dir}"
